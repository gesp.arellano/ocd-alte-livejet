<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Developed By:
        {{env('APP_DEVELOPER', 'Gabriel Arellano')}}
    </div>
    <!-- Default to the left -->
    <strong> 
        &copy;
        {{env('APP_COPYRIGHT_YEAR', 'xxxx')}}
        <a href="{{env('APP_COPYRIGHT_URL', '#')}}">
            {{env('APP_COPYRIGHT', '')}}
        </a>.
    </strong> 
    All rights reserved.
</footer>